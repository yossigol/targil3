<?php
class EX2{
    protected $title = "Example title";
    protected $body = "Example body";

    function __construct($title = "",$body = ""){
        if($title != ""){
            $this->title = $title;
        }
        if($body != ""){
            $this->body = $body;
        }
    }
    public function show(){
        echo "<html>
            <head>
                <title>$this->title</title>
            </head>
            <body>
                $this->body
            </body>
            </html>";
    }
}
class colored extends EX2{
    protected $pintura='red';
    public function __set($property,$value){
        if($property=='pintura'){
            $pintura= array('red','green','blue');
            if(in_array($value,$pintura)){
                    $this->pintura=$value;
      
            }
            else{
                    die("please select one of the allowed colors");
            }
        
        }
    }
    public function show(){
        echo "<html>
            <head>
                <title>$this->title</title>
            </head>
            <body>
                <p style= 'color:$this->pintura'>$this->body</p>
            </body>
            </html>";
    }
}
class EX3 extends colored{
    protected $size= 10;
    public function __set($property,$value){
        parent::__set($property,$value);
        if($property== 'size'){
            $numbers = array(10,11,12,13,14,15,16,17,18,19,20,21,22,23,24);
            if(in_array($value,$numbers)){
                $this->size = $value;
            }else {
                die("wrong font size");
            }
        }
    }
    public function show(){
        echo "<html>
            <head>
                <title>$this->title</title>
            </head>
            <body>
                <p style= 'color:$this->pintura;font-size:$this->size';>$this->body</p>
            </body>
            </html>";
    }
}
?>